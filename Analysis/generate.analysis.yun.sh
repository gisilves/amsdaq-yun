#!/bin/bash

DATADIR=/media/gigi/MaxtorGigi/FOOTDEC18/RootData/

for i in `ls  --ignore '*.pdf' ${DATADIR}`
do
    DECODE_ARG=$(echo $i | cut -d'_' -f 2| cut -d'.' -f 1)
    echo "${DECODE_ARG}_hits.root"

    ./AnalysisFOOT "${DECODE_ARG}_hits.root" ${DATADIR}/$i
done