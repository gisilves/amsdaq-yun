#include "TSystem.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TLine.h"
#include "TROOT.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TStopwatch.h"
#include <fstream>
#include <vector>
#include <Riostream.h>

/* from the 'Decode' API */
#include "Cluster.hh"
#include "Event.hh"
/* end */

/* from the CommonTool dir */
#include "TrackSelection.hh"
#include "Utilities.hh"
/* end */

using namespace std;

int main(int argc, const char *argv[])
{

  if (argc < 2)
  {
    printf("Usage:\n");
    printf("%s <output root-filename> <first input root-filename> [second input root-filename] ...\n", argv[0]);
    return 1;
  }

  // Initialize variables for S/N ang cog cuts
  //////////////////////////////////////////////

  TChain *chain = new TChain("t4");

  for (int ii = 2; ii < argc; ii++)
  {
    printf("Adding file %s to the chain...\n", argv[ii]);
    chain->Add(argv[ii]);
  }

  TString output_filename = argv[1];

  Event *ev;
  Cluster *cl;

  Long64_t entries = chain->GetEntries();
  printf("This run has %lld entries\n", entries);

  ev = new Event();
  chain->SetBranchAddress("cluster_branch", &ev);
  chain->GetEntry(0);

  int _maxtdr = NJINF * NTDRS;

  if (GetRH(chain))
  {
    GetRH(chain)->Print();
    _maxtdr = GetRH(chain)->ntdrCmp;
  }
  else
  {
    printf("Not able to find the RHClass header in the UserInfo...\n");
    return -9;
  }

  //  printf("%d\n", _maxladd);

  TFile *foutput = new TFile(output_filename.Data(), "RECREATE");
  foutput->cd();

  TStopwatch sw;
  sw.Start();

  double perc = 0;
  int zeroclust = 0;

  TTree *hitsMSD1 = new TTree("treeMSD1", "hitsMSD1");
  TTree *hitsMSD2 = new TTree("treeMSD2", "hitsMSD2");

  double xMSD1, yMSD1, xMSD2;

  hitsMSD1->Branch("x12", &xMSD1);
  hitsMSD1->Branch("y12", &yMSD1);
  hitsMSD2->Branch("x16", &xMSD2);

  for (int index_event = 0; index_event < entries; index_event++)
  {
    Double_t pperc = 1000.0 * ((index_event + 1.0) / entries);
    if (pperc >= perc)
    {
      printf("Processed %d out of %lld: %d%%\n", (index_event + 1), entries, (int)(100.0 * (index_event + 1.0) / entries));
      perc++;
    }
    chain->GetEntry(index_event);
    int NClusTot = ev->GetNClusTot();

    if (NClusTot != 3)
    {
      xMSD1 = -999;
      yMSD1 = -999;
      xMSD2 = -999;
    }

    for (int i = 0; i < NClusTot; i++)
    {
      cl = ev->GetCluster(i);
      int ladder = cl->ladder;
      int side = cl->side;
      double seedADC = cl->GetSeedVal();
      double seedPos = cl->GetSeedAdd();

      if (side == 0)
      {
        if (ladder == 12)
        {
          yMSD1 = seedPos;
        }
        else
        {
          xMSD2 = seedPos;
        }
      }
      else
      {
        if (ladder == 12)
        {
          xMSD1 = seedPos;
        }
      }
    }
    hitsMSD1->Fill();
    hitsMSD2->Fill();
  }

  sw.Stop();
  sw.Print();

  hitsMSD1->Write();
  hitsMSD2->Write();
  //foutput->Write();
  foutput->Close();

  // cout << "Zero clust events " << zeroclust << endl;
  return 0;
}
